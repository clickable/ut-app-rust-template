{% if cookiecutter.open_source_license == 'GNU General Public License v3' %}/*
 * Copyright (C) {% now 'local', '%Y'%}  {{cookiecutter.maintainer_name}}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

{% endif %}import QtQuick 2.7
//import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Greeter 1.0

ApplicationWindow {
    id: window

    width: 640
    height: 480

    title: "Ubuntu Touch ❤️ Rust"

    Greeter {
        id: greeter;
        name: "Rust + Ubuntu Touch"
    }

    StackView {
        anchors.fill: parent

        initialItem: Page {
            header: ToolBar {
                RowLayout {
                    anchors.fill: parent

                    Label {
                        text: "Ubuntu Touch ❤️ Rust"
                    }

                    Item { Layout.fillWidth: true }
                }
            }

            ColumnLayout {
                anchors {
                    fill: parent
                    margins: 16
                }

                spacing: 8

                Label {
                    id: label
                    text: "Press the button below!"
                }

                Button {
                    text: "Compute greeting"
                    onClicked: {
                        label.text = greeter.compute_greetings("Hello, ");
                    }
                }

                Item { Layout.fillHeight: true }
            }
        }
    }

    Component.onCompleted: {
        show();
    }
}
